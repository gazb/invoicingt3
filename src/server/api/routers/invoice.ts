import { z } from "zod";

import { createTRPCRouter, protectedProcedure } from "~/server/api/trpc";

export const invoiceRouter = createTRPCRouter({
  create: protectedProcedure
    .input(
      z.object({
        name: z.string().min(1),
        email: z.string().min(1),
        periodState: z.string().min(1),
        dateStart: z.string().min(1),
        dateEnd: z.string().min(1),
        description: z.string().min(1),
        dueDate: z.string().min(1),
        amount: z.string().min(1),
      }),
    )
    .mutation(async ({ ctx, input }) => {
      return ctx.db.rent.create({
        data: {
          name: input.name,
          email: input.email,
          periodState: input.periodState,
          dateStart: input.dateStart,
          dateEnd: input.dateEnd,
          description: input.description,
          dueDate: input.dueDate,
          amount: parseFloat(input.amount),
          status: input.amount,
          createdBy: { connect: { id: ctx.session.user.id } },
        },
      });
    }),

  getLatest: protectedProcedure.query(({ ctx }) => {
    return ctx.db.post.findFirst({
      orderBy: { createdAt: "desc" },
      where: { createdBy: { id: ctx.session.user.id } },
    });
  }),

  getSecretMessage: protectedProcedure.query(() => {
    return "you can now see this secret message!";
  }),
});
